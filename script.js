function myFunction($event) {
    $event.preventDefault();
    $(this).children('.dropdown-content').toggleClass('show');
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('.dropdown,.dropdown .selected-label,.dropdown img')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
};


$(window).ready(function () {
    $('.section-notes').slideUp(0);

    $('.toggle-note-list-btn').click(function () {
        $(this).children('img').toggleClass('up');
        $('.section-notes').slideToggle();
        setTimeout(function () {
            $('html, body').animate({
                    scrollTop: $('.toggle-note-list-btn').position().top},
                400
            );
        },400);

    });
    $('.prefix-number').html($('.option.selected').attr('data-prefix-number'));
    $('.dropdown').each(function () {
        $(this).children('.selected-label').html($(this).find('.option.selected').attr('data-value'));
        $(this).find('.option').each(function () {
            $(this).html($(this).attr('data-value')).click(function () {
                $(this).parents('.dropdown').children('.selected-label').html($(this).attr('data-value'));
                $(this).siblings().removeClass('selected');
                $(this).addClass('selected');
                $('.prefix-number').html($(this).attr('data-prefix-number'));
            });
        });
    });

    $('.dropdown').click(myFunction);
});
